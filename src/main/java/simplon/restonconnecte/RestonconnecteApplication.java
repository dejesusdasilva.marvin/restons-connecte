package simplon.restonconnecte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestonconnecteApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestonconnecteApplication.class, args);
	}

}
