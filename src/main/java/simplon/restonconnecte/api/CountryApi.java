package simplon.restonconnecte.api;

import org.springframework.web.bind.annotation.*;
import simplon.restonconnecte.model.Country;
import simplon.restonconnecte.service.CountryInterface;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class CountryApi {

    final private CountryInterface countryInterface;

    public CountryApi(CountryInterface countryInterface) {
        this.countryInterface = countryInterface;
    }

    @CrossOrigin("*")
    @GetMapping(path = "/countries")
    public List<Country> getCountries() {
        return countryInterface.countryList();
    }
    @CrossOrigin("*")
    @GetMapping(path = "/country/{id}")
    public Optional<Country> getCountryById(@PathVariable(value = "id") Long id_country) {
        return countryInterface.countryById(id_country);
    }
    @CrossOrigin("*")
    @PostMapping(path = "/country")
    public Country createCountry(@RequestBody Country country) {
        return countryInterface.insertCountry(country);
    }

    @CrossOrigin("*")
    @PutMapping(path = "/country/{id}")
    public Country updateCountry(@PathVariable(value = "id") Long id_country, @RequestBody Country country) {
        return countryInterface.updateCountry(id_country, country);
    }

    @CrossOrigin("*")
    @DeleteMapping("/country/{id}")
    public void deleteCountry(@PathVariable(value = "id") Long id_country){
        countryInterface.deleteCountry(id_country);
    }
}