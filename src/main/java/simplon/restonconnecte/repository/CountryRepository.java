package simplon.restonconnecte.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import simplon.restonconnecte.model.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
}
