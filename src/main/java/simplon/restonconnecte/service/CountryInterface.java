package simplon.restonconnecte.service;

import org.springframework.stereotype.Service;
import simplon.restonconnecte.model.Country;

import java.util.List;
import java.util.Optional;

@Service
public interface CountryInterface {
    List<Country> countryList ();
    Optional<Country> countryById(Long id_country);
    Country insertCountry(Country country);
    Country updateCountry(Long id_country, Country country);
    void deleteCountry(Long id_country);
}
