package simplon.restonconnecte.service;

import org.springframework.stereotype.Service;
import simplon.restonconnecte.model.Country;
import simplon.restonconnecte.repository.CountryRepository;

import java.util.*;

@Service
public class CountryInterfaceImplements implements CountryInterface {

    final private CountryRepository countryRepository;

    public CountryInterfaceImplements(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> countryList() {
        return countryRepository.findAll();
        }

    @Override
    public Optional<Country> countryById(Long id_country) {
         return countryRepository.findById(id_country);
    }


    @Override
    public Country insertCountry(Country country) {
        return countryRepository.save(country);
    }

    @Override
    public Country updateCountry(Long id_country, Country country) {
        Optional<Country> optionalCountry = this.countryRepository.findById(id_country);

        if (optionalCountry.isPresent()){
            Country countryToUpdate = optionalCountry.get();
            countryToUpdate.setCodeCountry(country.getCodeCountry());
            if (country.getCodeCountry() != null){
                countryToUpdate.setCodeCountry(country.getCodeCountry());
            }
            return countryRepository.save(country);
        }
        return null;
    }

    @Override
    public void deleteCountry(Long id_country) {
        Optional<Country> country = this.countryRepository.findById(id_country);
        country.ifPresent(countryRepository::delete);
    }


}
