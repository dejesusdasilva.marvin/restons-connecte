package simplon.restonconnecte.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue
    @Column(name = "id_country")
    private Long idCountry;
    @Column(name = "code_country")
    private String codeCountry;
    @Column(name = "name_country")
    private String nameCountry;

    public Country(Long idCountry, String codeCountry, String nameCountry) {
        this.idCountry = idCountry;
        this.codeCountry = codeCountry;
        this.nameCountry = nameCountry;
    }

    public Long getIdCountry() {
        return idCountry;
    }

    public String getCodeCountry() {
        return codeCountry;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setIdCountry(Long idCountry) {
        this.idCountry = idCountry;
    }

    public void setCodeCountry(String codeCountry) {
        this.codeCountry = codeCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }
}
